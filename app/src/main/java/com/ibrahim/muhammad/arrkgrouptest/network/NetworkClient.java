package com.ibrahim.muhammad.arrkgrouptest.network;

import com.ibrahim.muhammad.arrkgrouptest.network.network_model.*;
import com.ibrahim.muhammad.arrkgrouptest.utils.Constants;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by muhammadibrahim on 7/9/18.
 */

public class NetworkClient {

    Retrofit retrofit;
    NetworkService networkService;

    @Inject
    public NetworkClient(){

        retrofit = new Retrofit
                .Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        networkService = retrofit.create(NetworkService.class);
    }


    /**
     * Get an object of {@link StarWarsCharactersApiResponse}
     * containing list of {@link StarWarsCharacter}.
     * @param page
     * @param callback
     */
    public void getWarsCharacters(int page, Callback<StarWarsCharactersApiResponse> callback){
        Call<StarWarsCharactersApiResponse> call = networkService.getWarsCharacters(page);
        call.enqueue(callback);
    }
}
