package com.ibrahim.muhammad.arrkgrouptest.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ibrahim.muhammad.arrkgrouptest.R;
import com.ibrahim.muhammad.arrkgrouptest.databinding.ItemWarCharacterBinding;
import com.ibrahim.muhammad.arrkgrouptest.mvp.model.LoadingRecyclerItemHolder;
import com.ibrahim.muhammad.arrkgrouptest.mvp.model.MainCharacterRecyclerItemHolder;
import com.ibrahim.muhammad.arrkgrouptest.network.network_model.StarWarsCharacter;
import com.ibrahim.muhammad.arrkgrouptest.network.network_model.StarWarsCharactersApiResponse;
import com.ibrahim.muhammad.arrkgrouptest.utils.Constants;
import com.ibrahim.muhammad.arrkgrouptest.utils.WarItemClickListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by muhammadibrahim on 7/9/18.
 */

public class MainActivityRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    List<StarWarsCharacter> charactersData;
    WarItemClickListener warItemClickListener;


    private boolean isLoadingAdded = false;

    @Inject
    public MainActivityRecyclerAdapter() {
        this.charactersData = new ArrayList<>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if(viewType == Constants.ITEM_CHAR) {
            ItemWarCharacterBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_war_character, parent, false);
            return new MainCharacterRecyclerItemHolder(binding);
        }
        else{
            View view = inflater.inflate(R.layout.item_loading, parent, false);
            return new LoadingRecyclerItemHolder(view);
        }
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        switch (getItemViewType(position)){
            case Constants.ITEM_CHAR:
                ((MainCharacterRecyclerItemHolder)holder).getBinding().setCharacter(charactersData.get(position));
                ((MainCharacterRecyclerItemHolder)holder).getBinding().cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        warItemClickListener.onWarItemClicked(charactersData.get(position));
                    }
                });

                break;
            case Constants.ITEM_LOAD:
                break;
        }
    }


    @Override
    public int getItemViewType(int position) {
        if(position == charactersData.size() - 1 && isLoadingAdded){
            return Constants.ITEM_LOAD;
        }
        else{
            return Constants.ITEM_CHAR;
        }
    }

    @Override
    public int getItemCount() {
        return charactersData == null ? 0 : charactersData.size();
    }


    public void setWarItemClickListener(WarItemClickListener warItemClickListener) {
        this.warItemClickListener = warItemClickListener;
    }

    public void clearData(){
        this.charactersData.clear();
    }


    public void addAll(List<StarWarsCharacter> data){
        this.charactersData.addAll(data);
    }

    public void add(StarWarsCharacter character){
        charactersData.add(character);
    }

    public void remove(StarWarsCharacter character){
        int position = charactersData.indexOf(character);
        if(position > -1){
            charactersData.remove(position);
        }
    }

    public void addLoadingView(){
        isLoadingAdded = true;
        add(new StarWarsCharacter());
    }

    public void removeLoadingView(){
        isLoadingAdded = false;
        int position = charactersData.size() - 1;
        StarWarsCharacter character = getItem(position);
        if(character != null){
            remove(character);
        }
    }

    public StarWarsCharacter getItem(int position){
        return charactersData.get(position);
    }
}
