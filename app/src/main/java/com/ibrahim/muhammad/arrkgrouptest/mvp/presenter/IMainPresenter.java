package com.ibrahim.muhammad.arrkgrouptest.mvp.presenter;

import android.support.v7.widget.RecyclerView;

import com.ibrahim.muhammad.arrkgrouptest.mvp.view.IMainView;

/**
 * Created by muhammadibrahim on 7/9/18.
 */

public interface IMainPresenter {
    void attachView(IMainView mainView);
    void initRecyclerView(RecyclerView recyclerView);
    void loadFirstData();
    void loadMoreData();
}
