package com.ibrahim.muhammad.arrkgrouptest.dagger.modules;

import com.ibrahim.muhammad.arrkgrouptest.mvp.presenter.IMainPresenter;
import com.ibrahim.muhammad.arrkgrouptest.mvp.presenter.MainPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by muhammadibrahim on 7/12/18.
 */

@Module
public class MainActivityModule {

    @Provides
    IMainPresenter provideMainPresenter(MainPresenterImpl presenter){
        return presenter;
    }
}
