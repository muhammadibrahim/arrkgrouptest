package com.ibrahim.muhammad.arrkgrouptest.dagger.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by muhammadibrahim on 7/12/18.
 */

@Module
public class AppModule {
    Context context;

    public AppModule(Context context){
        this.context = context;
    }

    @Singleton
    @Provides
    Context provideAppContext(){
        return this.context;
    }
}
