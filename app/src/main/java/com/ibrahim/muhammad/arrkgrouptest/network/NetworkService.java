package com.ibrahim.muhammad.arrkgrouptest.network;

import com.ibrahim.muhammad.arrkgrouptest.network.network_model.StarWarsCharactersApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by muhammadibrahim on 7/9/18.
 */

public interface NetworkService {

    @GET("people")
    Call<StarWarsCharactersApiResponse> getWarsCharacters(@Query("page") int page);
}
