package com.ibrahim.muhammad.arrkgrouptest.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.ibrahim.muhammad.arrkgrouptest.R;

import com.ibrahim.muhammad.arrkgrouptest.databinding.ActivityDetailBinding;
import com.ibrahim.muhammad.arrkgrouptest.mvp.model.ViewModelDetailActivity;
import com.ibrahim.muhammad.arrkgrouptest.mvp.view.IDetailView;
import com.ibrahim.muhammad.arrkgrouptest.network.network_model.StarWarsCharacter;
import com.ibrahim.muhammad.arrkgrouptest.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by muhammadibrahim on 7/9/18.
 */

public class DetailActivity extends SuperActivity implements IDetailView{

    @BindView(R.id.detailToolbar)
    Toolbar detailToolbar;

    ActivityDetailBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);

        ButterKnife.bind(this);
        initActionBar();
        initView();
    }

    private void initActionBar() {
        setSupportActionBar(detailToolbar);
    }

    private void initView(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        StarWarsCharacter warsCharacter;

        if(bundle != null){
            warsCharacter = bundle.getParcelable(Constants.WAR_CHARACHTER);
            ViewModelDetailActivity character = new ViewModelDetailActivity(warsCharacter);
            binding.setCharacter(character);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    void closeActivity(){
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }
}
