package com.ibrahim.muhammad.arrkgrouptest.mvp.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by muhammadibrahim on 7/10/18.
 */

public class LoadingRecyclerItemHolder extends RecyclerView.ViewHolder {
    public LoadingRecyclerItemHolder(View itemView) {
        super(itemView);
    }
}
