package com.ibrahim.muhammad.arrkgrouptest.dagger.components;

import android.content.Context;

import com.ibrahim.muhammad.arrkgrouptest.dagger.modules.AppModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Module;

/**
 * Created by muhammadibrahim on 7/12/18.
 */

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
