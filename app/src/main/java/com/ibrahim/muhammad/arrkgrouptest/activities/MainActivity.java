package com.ibrahim.muhammad.arrkgrouptest.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.ibrahim.muhammad.arrkgrouptest.R;
import com.ibrahim.muhammad.arrkgrouptest.adapters.MainActivityRecyclerAdapter;
import com.ibrahim.muhammad.arrkgrouptest.dagger.components.DaggerMainActivityComponent;
import com.ibrahim.muhammad.arrkgrouptest.dagger.modules.MainActivityModule;
import com.ibrahim.muhammad.arrkgrouptest.mvp.presenter.IMainPresenter;
import com.ibrahim.muhammad.arrkgrouptest.mvp.presenter.MainPresenterImpl;
import com.ibrahim.muhammad.arrkgrouptest.mvp.view.IMainView;
import com.ibrahim.muhammad.arrkgrouptest.network.network_model.StarWarsCharacter;
import com.ibrahim.muhammad.arrkgrouptest.utils.ArrkGroupApp;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends SuperActivity implements IMainView, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    MainPresenterImpl presenter;

    @BindView(R.id.charactersList)
    RecyclerView charactersList;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.llNetworkInfo)
    LinearLayout llNetworkInfl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        DaggerMainActivityComponent.builder()
                .appComponent(ArrkGroupApp.getComponent())
                .mainActivityModule(new MainActivityModule())
                .build()
                .inject(this);

        swipeRefreshLayout.setOnRefreshListener(this);
        presenter.attachView(this);
        presenter.initRecyclerView(charactersList);

        loadData();
    }

    private void loadData(){
        if(isNetworkConnected()){
            showProgress();
            presenter.loadFirstData();
        }
        else{
            showFailure();
            setRefreshing(false);
        }
    }

    @OnClick(R.id.btnTryAgain)
    public void tryAgainBtnClicked(View view){
        loadData();
    }

    @Override
    public void showFailure() {
        charactersList.setVisibility(View.GONE);
        llNetworkInfl.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        charactersList.setVisibility(View.VISIBLE);
        llNetworkInfl.setVisibility(View.GONE);
    }

    @Override
    public void onRefresh() {
        loadData();
    }

    @Override
    public void setRefreshing(boolean isRefreshing) {
        swipeRefreshLayout.setRefreshing(isRefreshing);
    }
}

