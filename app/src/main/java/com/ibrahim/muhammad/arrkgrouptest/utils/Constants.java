package com.ibrahim.muhammad.arrkgrouptest.utils;

/**
 * Created by muhammadibrahim on 7/9/18.
 */

public class Constants {
    public static final String BASE_URL = "https://swapi.co/api/";
    public static final int ITEM_CHAR = 0;
    public static final int ITEM_LOAD = 1;
    public static final String WAR_CHARACHTER = "warCharacter";
}
