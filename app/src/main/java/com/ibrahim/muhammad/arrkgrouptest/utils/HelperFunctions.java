package com.ibrahim.muhammad.arrkgrouptest.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by muhammadibrahim on 7/12/18.
 */

public final class HelperFunctions {

    public static String getFormattedDateTime(String timestamp, SimpleDateFormat desiredFormat){

        String formattedDateTime = "";

        if (timestamp != null) {
            SimpleDateFormat timpestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            timpestampFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date date = null;
            try {
                date = timpestampFormat.parse(timestamp);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (date != null) {
                desiredFormat.setTimeZone(TimeZone.getDefault());
                formattedDateTime = desiredFormat.format(date);
            }
        }
        return formattedDateTime;
    }

    public static String centimetersToMeters(String centimeters){
        try {
            double centi = Double.parseDouble(centimeters);
            double meters = centi / 100;
            return String.valueOf(meters);
        }
        catch (Exception e){
            return centimeters;
        }
    }

}
