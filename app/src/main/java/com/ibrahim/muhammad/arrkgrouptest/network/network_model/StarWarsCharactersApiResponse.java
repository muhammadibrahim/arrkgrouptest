package com.ibrahim.muhammad.arrkgrouptest.network.network_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by muhammadibrahim on 7/9/18.
 */

public class StarWarsCharactersApiResponse {

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("next")
    @Expose
    private String next;
    @SerializedName("previous")
    @Expose
    private String previous;
    @SerializedName("results")
    @Expose
    private List<StarWarsCharacter> characterList = null;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public List<StarWarsCharacter> getCharacterList() {
        return characterList;
    }

    public void setCharacterList(List<StarWarsCharacter> characterList) {
        this.characterList = characterList;
    }
}

