package com.ibrahim.muhammad.arrkgrouptest.utils;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.ibrahim.muhammad.arrkgrouptest.dagger.components.AppComponent;
import com.ibrahim.muhammad.arrkgrouptest.dagger.components.DaggerAppComponent;
import com.ibrahim.muhammad.arrkgrouptest.dagger.modules.AppModule;

/**
 * Created by muhammadibrahim on 7/12/18.
 */

public class ArrkGroupApp extends Application {
    private static AppComponent component;

    public ArrkGroupApp(){
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent getComponent() {
        return component;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}
