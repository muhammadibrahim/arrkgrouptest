package com.ibrahim.muhammad.arrkgrouptest.network.network_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ibrahim.muhammad.arrkgrouptest.utils.HelperFunctions;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by muhammadibrahim on 7/9/18.
 */

public class StarWarsCharacter implements Parcelable{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("mass")
    @Expose
    private String mass;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("edited")
    @Expose
    private String edited;


    private SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aaa");
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
    private String createdDate;
    private String createdTime;

    public StarWarsCharacter(){}

    protected StarWarsCharacter(Parcel in) {
        name = in.readString();
        height = in.readString();
        mass = in.readString();
        created = in.readString();
        edited = in.readString();
    }

    public static final Creator<StarWarsCharacter> CREATOR = new Creator<StarWarsCharacter>() {
        @Override
        public StarWarsCharacter createFromParcel(Parcel in) {
            return new StarWarsCharacter(in);
        }

        @Override
        public StarWarsCharacter[] newArray(int size) {
            return new StarWarsCharacter[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getMass() {
        return mass;
    }

    public void setMass(String mass) {
        this.mass = mass;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getEdited() {
        return edited;
    }

    public void setEdited(String edited) {
        this.edited = edited;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(height);
        dest.writeString(mass);
        dest.writeString(created);
        dest.writeString(edited);
    }

    public String getCreatedDate() {
        return HelperFunctions.getFormattedDateTime(this.created, dateFormat);
    }

    public String getCreatedTime() {
        return HelperFunctions.getFormattedDateTime(this.created, timeFormat);
    }
}