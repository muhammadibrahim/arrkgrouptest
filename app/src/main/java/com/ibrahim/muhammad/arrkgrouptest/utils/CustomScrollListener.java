package com.ibrahim.muhammad.arrkgrouptest.utils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

/**
 * Created by muhammadibrahim on 7/10/18.
 */

public abstract class CustomScrollListener extends RecyclerView.OnScrollListener {

    LinearLayoutManager layoutManager;

    public CustomScrollListener(LinearLayoutManager layoutManager){
        this.layoutManager = layoutManager;
    }
    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        int visibleItemCount = layoutManager.getChildCount();
        int currentItemsCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

        if (!isLoading() && !isLastPage()) {
            if ((visibleItemCount + firstVisibleItemPosition) >= currentItemsCount
                    && firstVisibleItemPosition >= 0
                    && currentItemsCount < getAllItemsCount()) {
                Log.d("reach_end", "Reach to the end");
                loadMoreItems();
            }
            else{
                Log.d("reach_end", "Not end");
            }
        }
    }

    protected abstract void loadMoreItems();

    public abstract int getAllItemsCount();

    public abstract boolean isLastPage();

    public abstract boolean isLoading();

}
