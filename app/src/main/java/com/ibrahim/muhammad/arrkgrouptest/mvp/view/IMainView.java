package com.ibrahim.muhammad.arrkgrouptest.mvp.view;

/**
 * Created by muhammadibrahim on 7/9/18.
 */

public interface IMainView {
    void showProgress();
    void showFailure();
    void setRefreshing(boolean isRefreshing);
}
