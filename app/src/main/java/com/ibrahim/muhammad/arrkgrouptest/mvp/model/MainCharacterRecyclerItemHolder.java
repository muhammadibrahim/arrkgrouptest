package com.ibrahim.muhammad.arrkgrouptest.mvp.model;

import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.ibrahim.muhammad.arrkgrouptest.databinding.ItemWarCharacterBinding;

/**
 * Created by muhammadibrahim on 7/9/18.
 */

public class MainCharacterRecyclerItemHolder extends RecyclerView.ViewHolder{

    private ItemWarCharacterBinding binding;

    public MainCharacterRecyclerItemHolder(ItemWarCharacterBinding binding){
        super(binding.getRoot());
        this.binding = binding;
    }

    public ItemWarCharacterBinding getBinding() {
        return binding;
    }
}
