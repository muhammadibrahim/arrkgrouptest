package com.ibrahim.muhammad.arrkgrouptest.mvp.model;

import android.databinding.BaseObservable;

import com.ibrahim.muhammad.arrkgrouptest.network.network_model.StarWarsCharacter;
import com.ibrahim.muhammad.arrkgrouptest.utils.HelperFunctions;

import java.text.SimpleDateFormat;

/**
 * Created by muhammadibrahim on 7/12/18.
 */

public class ViewModelDetailActivity extends BaseObservable {

    private String name;
    private String height;
    private String dateCreated;
    private String timeCreated;
    private String massInKg;

    private SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aaa");
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");


    public ViewModelDetailActivity(StarWarsCharacter character){
        this.name = character.getName();
        this.height = HelperFunctions.centimetersToMeters(character.getHeight());
        this.massInKg = character.getMass();
        this.dateCreated = HelperFunctions.getFormattedDateTime(character.getCreated(), dateFormat);
        this.timeCreated = HelperFunctions.getFormattedDateTime(character.getCreated(), timeFormat);
    }

    public String getName() {
        return name;
    }

    public String getHeight() {
        return height;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public String getTimeCreated() {
        return timeCreated;
    }

    public String getMassInKg() {
        return massInKg;
    }
}
