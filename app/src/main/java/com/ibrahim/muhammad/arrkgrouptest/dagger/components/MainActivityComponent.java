package com.ibrahim.muhammad.arrkgrouptest.dagger.components;

import com.ibrahim.muhammad.arrkgrouptest.activities.MainActivity;
import com.ibrahim.muhammad.arrkgrouptest.dagger.modules.MainActivityModule;
import com.ibrahim.muhammad.arrkgrouptest.dagger.scopes.ActivityScope;

import dagger.Component;

/**
 * Created by muhammadibrahim on 7/12/18.
 */

@ActivityScope
@Component(modules = MainActivityModule.class, dependencies = AppComponent.class)
public interface MainActivityComponent {
    void inject(MainActivity mainActivity);
}
