package com.ibrahim.muhammad.arrkgrouptest.mvp.presenter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ibrahim.muhammad.arrkgrouptest.R;
import com.ibrahim.muhammad.arrkgrouptest.activities.DetailActivity;
import com.ibrahim.muhammad.arrkgrouptest.activities.MainActivity;
import com.ibrahim.muhammad.arrkgrouptest.adapters.MainActivityRecyclerAdapter;
import com.ibrahim.muhammad.arrkgrouptest.mvp.view.IMainView;
import com.ibrahim.muhammad.arrkgrouptest.network.NetworkClient;
import com.ibrahim.muhammad.arrkgrouptest.network.network_model.StarWarsCharacter;
import com.ibrahim.muhammad.arrkgrouptest.network.network_model.StarWarsCharactersApiResponse;
import com.ibrahim.muhammad.arrkgrouptest.utils.Constants;
import com.ibrahim.muhammad.arrkgrouptest.utils.CustomScrollListener;
import com.ibrahim.muhammad.arrkgrouptest.utils.WarItemClickListener;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by muhammadibrahim on 7/9/18.
 */

public class MainPresenterImpl implements IMainPresenter {

    IMainView mainView;

    @Inject
    NetworkClient networkClient;
    @Inject
    MainActivityRecyclerAdapter adapter;

    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int START_PAGE = 1;
    private int NEXT_PAGE = START_PAGE;
    private int ALL_ITEMS_COUNT = 0;


    @Inject
    public MainPresenterImpl(){
    }

    @Override
    public void attachView(IMainView mainView) {
        this.mainView = mainView;
    }

    @Override
    public void initRecyclerView(RecyclerView recyclerView) {

        adapter.setWarItemClickListener(listener);
        LinearLayoutManager layoutManager = new LinearLayoutManager((Context) mainView, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new CustomScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                loadMoreData();
            }

            @Override
            public int getAllItemsCount() {
                return ALL_ITEMS_COUNT;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    @Override
    public void loadFirstData() {
        Callback<StarWarsCharactersApiResponse> callback = new Callback<StarWarsCharactersApiResponse>() {

            @Override
            public void onResponse(Call<StarWarsCharactersApiResponse> call, Response<StarWarsCharactersApiResponse> response) {
                if(response.isSuccessful()){
                    adapter.clearData();
                    handleResponse(response);
                }
                mainView.setRefreshing(false);
                isLoading = false;
            }

            @Override
            public void onFailure(Call<StarWarsCharactersApiResponse> call, Throwable t) {
                mainView.setRefreshing(false);
                mainView.showFailure();
                isLoading = false;
            }
        };

        isLastPage = false;
        mainView.setRefreshing(true);
        networkClient.getWarsCharacters(START_PAGE, callback);
    }

    @Override
    public void loadMoreData() {

        Callback<StarWarsCharactersApiResponse> callback = new Callback<StarWarsCharactersApiResponse>() {

            @Override
            public void onResponse(Call<StarWarsCharactersApiResponse> call, Response<StarWarsCharactersApiResponse> response) {
                if(response.isSuccessful()){
                    adapter.removeLoadingView();
                    handleResponse(response);
                }
                isLoading = false;
            }

            @Override
            public void onFailure(Call<StarWarsCharactersApiResponse> call, Throwable t) {
                isLoading = false;
                adapter.removeLoadingView();
                adapter.notifyDataSetChanged();
            }
        };

        networkClient.getWarsCharacters(NEXT_PAGE, callback);
    }

    private void handleResponse(Response<StarWarsCharactersApiResponse> response){

        adapter.addAll(response.body().getCharacterList());

        ALL_ITEMS_COUNT = response.body().getCount();

        String nextPageUrl = response.body().getNext();

        if(nextPageUrl != null) {
            String nextPage = nextPageUrl.substring(nextPageUrl.indexOf("=") + 1);
            NEXT_PAGE = Integer.parseInt(nextPage);
            adapter.addLoadingView();
        }
        else{
            isLastPage = true;
        }
        adapter.notifyDataSetChanged();
    }


    //region Listener
    WarItemClickListener listener = new WarItemClickListener() {
        @Override
        public void onWarItemClicked(StarWarsCharacter character) {
            Intent intent = new Intent((Context) mainView, DetailActivity.class);
            intent.putExtra(Constants.WAR_CHARACHTER, character);
            ((MainActivity)mainView).startActivity(intent);
            ((MainActivity)mainView).overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        }
    };
    //endregion
}
