package com.ibrahim.muhammad.arrkgrouptest.utils;

import com.ibrahim.muhammad.arrkgrouptest.network.network_model.StarWarsCharacter;

/**
 * Created by muhammadibrahim on 7/12/18.
 */

public interface WarItemClickListener {
    void onWarItemClicked(StarWarsCharacter character);
}
